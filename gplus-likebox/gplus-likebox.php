<?php 
/*
   Plugin Name: Google Plus Followbox
   Plugin URI: http://musardos.com.br/plugingplus/Configuracao
   Description: This plugin allows you to add a widget with information and iterations of your profile or page of Google Plus similar to Facebook Like Box
   Version: 1.0
   Author: Musardo & Eduardo Abreu
   Author URI: http://musardos.com.br/
   License: GPL2
   */

   class gplus_likebox extends WP_Widget{

   		function __construct(){
   			parent::__construct('gplus_likebox','Google Plus Follow Box',array('description'=>'Follow box Google Plus'));
   		}

   		function form($instance)
   		{
   			if($instance){
   				$title = esc_attr($instance['title']);
   				$gid = esc_attr($instance['gid']);
   				$width = esc_attr($instance['width']);
   				$showFaces = $instance['showFaces'];
   				$showStream = $instance['showStream'];

   			}else{
   				$title = '';
   				$gid = '';
   				$width = '';
   				$showFaces = 1;
   				$showStream = 1;
   			}

   			##Widget adm form
   			?>
   			<?php     
			wp_enqueue_style('thickbox');
			wp_enqueue_script('thickbox');    
			?>
   			<script type="text/javascript">
   			jQuery(document).ready(function(){
   				jQuery('.openImage').click(function(){
   					tb_show('Find a Google Plus ID',jQuery(this).attr('href'));
   					return false;
   				});

   			});</script>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title','wp_gplus_likebox'); ?></label><br />
				<input class="widfat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"  /> 
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('gid'); ?>"><?php _e('Google+ ID','wp_gplus_likebox'); ?></label><br />
				<input class="widfat" id="<?php echo $this->get_field_id('gid'); ?>" name="<?php echo $this->get_field_name('gid'); ?>" type="text" value="<?php echo $gid; ?>"  /><br /> 
				<small><?php _e("Don't know where find it?",'wp_gplus_likebox'); ?> <a class="openImage" href="http://musardos.com.br/plugingplus/Content/where_get_googleplus_id.png"><?php _e('Look here.','wp_gplus_likebox') ?></a></small>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('width'); ?>"><?php _e('Width','wp_gplus_likebox'); ?></label><br />
				<input type="text" class="widfat" id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>" value="<?php echo $width; ?>" /> 
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('showFaces'); ?>"><input class="widfat" id="<?php echo $this->get_field_id('showFaces'); ?>" name="<?php echo $this->get_field_name('showFaces'); ?>" type="checkbox" value='1' <?php echo ($showFaces==1)? 'checked="checked"' : ''; ?> /> <?php _e('Show Faces','wp_gplus_likebox'); ?> </label><br />
				 
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('showStream'); ?>"><input class="widfat" id="<?php echo $this->get_field_id('showStream'); ?>" name="<?php echo $this->get_field_name('showStream'); ?>" type="checkbox" value='1' <?php echo ($showStream==1)? 'checked="checked"' : ''; ?> /> <?php _e('Show Stream','wp_gplus_likebox'); ?></label><br />
				 
			</p>
			<hr />

   			<?php

   		}


   		function update($new_instance,$old_instance)
   		{
   			$instance = $old_instance;
   			//fields
   			$instance['title'] = strip_tags($new_instance['title']);
   			$instance['gid'] = strip_tags($new_instance['gid']);
   			$instance['width'] = strip_tags($new_instance['width']);
   			$instance['showFaces'] = strip_tags($new_instance['showFaces']);
   			$instance['showStream'] = strip_tags($new_instance['showStream']);

   			return $instance;
   		}

   		//Display the widget
   		function widget($args, $instance){
   			extract($args);
   			$title = apply_filters('widget_title',$instance['title']);
   			$gid = $instance['gid'];
   			$width = $instance['width'];
   			$showFaces = $instance['showFaces'];
   			$showStream = $instance['showStream'];
   			$height = 400;

   			if($showFaces!=1 && $showStream!=1)
   			{
   				$height = 95;	

   			}elseif($showFaces==1 && $showStream!=1)
   			{
   				$height = 155;	
   			}elseif($showFaces!=1 && $showStream==1)
   			{
   				$height = 340;
   			}else{
   				$height = 400;
   			}

   			echo $before_widget;
   			?>
   			<iframe id="ifrGoogle" src="http://musardos.com.br/plugingplus/?width=<?php echo $width; ?>&amp;plusid=<?php echo $gid; ?>&amp;items=5&amp;showfaces=<?php echo $showFaces; ?>&amp;showstream=<?php echo $showStream; ?>" scrolling="no" frameborder="0" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px;"></iframe>
   			<?php echo $after_widget;
   		}

   }


   //register widget

   add_action('widgets_init',create_function('','register_widget("gplus_likebox");'));
?>